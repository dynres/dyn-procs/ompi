/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2004-2005 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2004-2005 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2004-2005 High Performance Computing Center Stuttgart,
 *                         University of Stuttgart.  All rights reserved.
 * Copyright (c) 2004-2005 The Regents of the University of California.
 *                         All rights reserved.
 * Copyright (c) 2011-2013 Cisco Systems, Inc.  All rights reserved.
 * Copyright (c) 2013      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * Copyright (c) 2015      Research Organization for Information Science
 *                         and Technology (RIST). All rights reserved.
 * Copyright (c) 2019      Triad National Security, LLC.  All rights reserved.
 *
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi_config.h"

#include "ompi/mpi/fortran/mpif-h/bindings.h"
#include "ompi/mpi/fortran/base/fortran_base_strings.h"
#include "ompi/constants.h"
#include "ompi/instance/instance.h"


#if OMPI_BUILD_MPI_PROFILING
#if OPAL_HAVE_WEAK_SYMBOLS
#pragma weak PMPI_SESSION_SET_PSET_DATA = ompi_session_set_pset_data_f
#pragma weak pmpi_session_set_pset_data = ompi_session_set_pset_data_f
#pragma weak pmpi_session_set_pset_data_ = ompi_session_set_pset_data_f
#pragma weak pmpi_session_set_pset_data__ = ompi_session_set_pset_data_f

#pragma weak PMPI_Session_set_pset_data_f = ompi_session_set_pset_data_f
#pragma weak PMPI_Session_set_pset_data_f08 = ompi_session_set_pset_data_f
#else
OMPI_GENERATE_F77_BINDINGS (PMPI_SESSION_SET_PSET_DATA,
                            pmpi_session_set_pset_data,
                            pmpi_session_set_pset_data_,
                            pmpi_session_set_pset_data__,
                            pompi_session_set_pset_data_f,
                            (MPI_Fint *session, char *pset_name, MPI_Fint *info_used, MPI_Fint *ierr, int pset_name_len),
                            (session, pset_name, info_used, ierr, pset_name_len))
#endif
#endif

#if OPAL_HAVE_WEAK_SYMBOLS
#pragma weak MPI_SESSION_SET_PSET_DATA = ompi_session_set_pset_data_f
#pragma weak mpi_session_set_pset_data = ompi_session_set_pset_data_f
#pragma weak mpi_session_set_pset_data_ = ompi_session_set_pset_data_f
#pragma weak mpi_session_set_pset_data__ = ompi_session_set_pset_data_f

#pragma weak MPI_Session_set_pset_data_f = ompi_session_set_pset_data_f
#pragma weak MPI_Session_set_pset_data_f08 = ompi_session_set_pset_data_f
#else
#if ! OMPI_BUILD_MPI_PROFILING
OMPI_GENERATE_F77_BINDINGS (MPI_SESSION_SET_PSET_DATA,
                            mpi_session_set_pset_data,
                            mpi_session_set_pset_data_,
                            mpi_session_set_pset_data__,
                            ompi_session_set_pset_data_f,
                            (MPI_Fint *session, char *pset_name, MPI_Fint *info_used, MPI_Fint *ierr, int pset_name_len),
                            (session, pset_name, info_used, ierr, pset_name_len))
#else
#define ompi_session_set_pset_data_f pompi_session_set_pset_data_f
#endif
#endif

void ompi_session_set_pset_data_f(MPI_Fint *session, char *pset_name, MPI_Fint *info_used,
                                  MPI_Fint *ierr, int pset_name_len)
{
    MPI_Session c_session;
    char *c_pset_name;
    MPI_Info c_info_used;
    int c_ierr = 0;

    c_session = PMPI_Session_f2c(*session);
    c_info_used = PMPI_Info_f2c(*info_used);

    if (MPI_PARAM_CHECK) {
        if (NULL == c_session || MPI_SESSION_NULL == c_session) {
            if (NULL != ierr) {
                *ierr = OMPI_INT_2_FINT(MPI_ERR_ARG);
            }
        }
    }

    // convert string
    if (OMPI_SUCCESS
        != (c_ierr = ompi_fortran_string_f2c(pset_name, pset_name_len, &c_pset_name))) {
        if (NULL != ierr) {
            *ierr = OMPI_INT_2_FINT(c_ierr);
        }
        return;
    }

    c_ierr = ompi_instance_set_pset_info((ompi_instance_t *) c_session, c_pset_name, (ompi_info_t *) c_info_used);

    // return value
    if (NULL != ierr) {
        *ierr = OMPI_INT_2_FINT(c_ierr);
    }

    // free memory
    free(c_pset_name);
}
