/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2004-2005 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2004-2005 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2004-2005 High Performance Computing Center Stuttgart,
 *                         University of Stuttgart.  All rights reserved.
 * Copyright (c) 2004-2005 The Regents of the University of California.
 *                         All rights reserved.
 * Copyright (c) 2011-2013 Cisco Systems, Inc.  All rights reserved.
 * Copyright (c) 2013      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * Copyright (c) 2015      Research Organization for Information Science
 *                         and Technology (RIST). All rights reserved.
 * Copyright (c) 2019      Triad National Security, LLC.  All rights reserved.
 *
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi_config.h"

#include "ompi/constants.h"
#include "ompi/instance/instance.h"
#include "ompi/mpi/fortran/base/fortran_base_strings.h"
#include "ompi/mpi/fortran/mpif-h/bindings.h"

#if OMPI_BUILD_MPI_PROFILING
#    if OPAL_HAVE_WEAK_SYMBOLS
#        pragma weak PMPI_SESSION_DYN_V2A_QUERY_PSETOP_NB = ompi_session_dyn_v2a_query_psetop_nb_f
#        pragma weak pmpi_session_dyn_v2a_query_psetop_nb = ompi_session_dyn_v2a_query_psetop_nb_f
#        pragma weak pmpi_session_dyn_v2a_query_psetop_nb_ = ompi_session_dyn_v2a_query_psetop_nb_f
#        pragma weak pmpi_session_dyn_v2a_query_psetop_nb__ = ompi_session_dyn_v2a_query_psetop_nb_f

#        pragma weak PMPI_Session_dyn_v2a_query_psetop_nb_f = ompi_session_dyn_v2a_query_psetop_nb_f
#        pragma weak PMPI_Session_dyn_v2a_query_psetop_nb_f08 \
            = ompi_session_dyn_v2a_query_psetop_nb_f
#    else
OMPI_GENERATE_F77_BINDINGS(PMPI_SESSION_DYN_V2A_QUERY_PSETOP_NB,
                           pmpi_session_dyn_v2a_query_psetop_nb,
                           pmpi_session_dyn_v2a_query_psetop_nb_,
                           pmpi_session_dyn_v2a_query_psetop_nb__,
                           pompi_session_dyn_v2a_query_psetop_nb_f,
                           (MPI_Fint * session, char *coll_pset, char *input_pset, MPI_Fint *type,
                            char *output_psets, MPI_Fint *noutput, MPI_Fint *request,
                            MPI_Fint *ierr, int coll_pset_len, int input_pset_len,
                            int output_psets_strlen),
                           (session, coll_pset, input_pset, type, output_pset, noutput, request,
                            ierr, coll_pset_len, input_pset_len, output_psets_strlen))
#    endif
#endif

#if OPAL_HAVE_WEAK_SYMBOLS
#    pragma weak MPI_SESSION_DYN_V2A_QUERY_PSETOP_NB = ompi_session_dyn_v2a_query_psetop_nb_f
#    pragma weak mpi_session_dyn_v2a_query_psetop_nb = ompi_session_dyn_v2a_query_psetop_nb_f
#    pragma weak mpi_session_dyn_v2a_query_psetop_nb_ = ompi_session_dyn_v2a_query_psetop_nb_f
#    pragma weak mpi_session_dyn_v2a_query_psetop_nb__ = ompi_session_dyn_v2a_query_psetop_nb_f

#    pragma weak MPI_Session_dyn_v2a_query_psetop_nb_f = ompi_session_dyn_v2a_query_psetop_nb_f
#    pragma weak MPI_Session_dyn_v2a_query_psetop_nb_f08 = ompi_session_dyn_v2a_query_psetop_nb_f
#else
#    if !OMPI_BUILD_MPI_PROFILING
OMPI_GENERATE_F77_BINDINGS(MPI_SESSION_DYN_V2A_QUERY_PSETOP_NB, mpi_session_dyn_v2a_query_psetop_nb,
                           mpi_session_dyn_v2a_query_psetop_nb_,
                           mpi_session_dyn_v2a_query_psetop_nb__,
                           ompi_session_dyn_v2a_query_psetop_nb_f,
                           (MPI_Fint * session, char *coll_pset, char *input_pset, MPI_Fint *type,
                            char *output_psets, MPI_Fint *noutput, MPI_Fint *request,
                            MPI_Fint *ierr, int coll_pset_len, int input_pset_len,
                            int output_psets_strlen),
                           (session, coll_pset, input_pset, type, output_pset, noutput, request,
                            ierr, coll_pset_len, input_pset_len, output_psets_strlen))
#    else
#        define ompi_session_dyn_v2a_query_psetop_nb_f pompi_session_dyn_v2a_query_psetop_nb_f
#    endif
#endif

void ompi_session_dyn_v2a_query_psetop_nb_f(MPI_Fint *session, char *coll_pset, char *input_pset,
                                            MPI_Fint *type, char *output_psets, MPI_Fint *noutput,
                                            MPI_Fint *request, MPI_Fint *ierr, int coll_pset_len,
                                            int input_pset_len, int output_psets_strlen)
{
    // this can not be implemented right now
}
