/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2004-2005 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2004-2005 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2004-2005 High Performance Computing Center Stuttgart,
 *                         University of Stuttgart.  All rights reserved.
 * Copyright (c) 2004-2005 The Regents of the University of California.
 *                         All rights reserved.
 * Copyright (c) 2011-2013 Cisco Systems, Inc.  All rights reserved.
 * Copyright (c) 2013      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * Copyright (c) 2015      Research Organization for Information Science
 *                         and Technology (RIST). All rights reserved.
 * Copyright (c) 2019      Triad National Security, LLC.  All rights reserved.
 *
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi_config.h"

#include "ompi/constants.h"
#include "ompi/instance/instance.h"
#include "ompi/mpi/fortran/base/fortran_base_strings.h"
#include "ompi/mpi/fortran/mpif-h/bindings.h"
#include "opal/util/argv.h"

#if OMPI_BUILD_MPI_PROFILING
#    if OPAL_HAVE_WEAK_SYMBOLS
#        pragma weak PMPI_SESSION_GET_PSET_DATA_NB = ompi_session_get_pset_data_nb_f
#        pragma weak pmpi_session_get_pset_data_nb = ompi_session_get_pset_data_nb_f
#        pragma weak pmpi_session_get_pset_data_nb_ = ompi_session_get_pset_data_nb_f
#        pragma weak pmpi_session_get_pset_data_nb__ = ompi_session_get_pset_data_nb_f

#        pragma weak PMPI_Session_get_pset_data_nb_f = ompi_session_get_pset_data_nb_f
#        pragma weak PMPI_Session_get_pset_data_nb_f08 = ompi_session_get_pset_data_nb_f
#    else
OMPI_GENERATE_F77_BINDINGS(PMPI_SESSION_GET_PSET_DATA_NB, pmpi_session_get_pset_data_nb,
                           pmpi_session_get_pset_data_nb_, pmpi_session_get_pset_data_nb__,
                           pompi_session_get_pset_data_nb_f,
                           (MPI_Fint * session, char *pset_name, char *coll_pset_name,
                            char *keys, MPI_Fint *nkeys, MPI_Fint *wait, MPI_Fint *info_used,
                            MPI_Fint *request, MPI_Fint *ierr, int coll_pset_name_len,
                            int pset_name_len, int keys_strlen),
                           (session, coll_pset_name, keys, nkeys, wait, info_used, request, ierr,
                            coll_pset_name_len, pset_name_len, keys_strlen))
#    endif
#endif

#if OPAL_HAVE_WEAK_SYMBOLS
#    pragma weak MPI_SESSION_GET_PSET_DATA_NB = ompi_session_get_pset_data_nb_f
#    pragma weak mpi_session_get_pset_data_nb = ompi_session_get_pset_data_nb_f
#    pragma weak mpi_session_get_pset_data_nb_ = ompi_session_get_pset_data_nb_f
#    pragma weak mpi_session_get_pset_data_nb__ = ompi_session_get_pset_data_nb_f

#    pragma weak MPI_Session_get_pset_data_nb_f = ompi_session_get_pset_data_nb_f
#    pragma weak MPI_Session_get_pset_data_nb_f08 = ompi_session_get_pset_data_nb_f
#else
#    if !OMPI_BUILD_MPI_PROFILING
OMPI_GENERATE_F77_BINDINGS(MPI_SESSION_GET_PSET_DATA_NB, mpi_session_get_pset_data_nb,
                           mpi_session_get_pset_data_nb_, mpi_session_get_pset_data_nb__,
                           ompi_session_get_pset_data_nb_f,
                           (MPI_Fint * session, char *pset_name, char *coll_pset_name,
                            char *keys, MPI_Fint *nkeys, MPI_Fint *wait, MPI_Fint *info_used,
                            MPI_Fint *request, MPI_Fint *ierr, int coll_pset_name_len,
                            int pset_name_len, int keys_strlen),
                           (session, coll_pset_name, keys, nkeys, wait, info_used, request, ierr,
                            coll_pset_name_len, pset_name_len, keys_strlen))
#    else
#        define ompi_session_get_pset_data_nb_f pompi_session_get_pset_data_nb_f
#    endif
#endif

void ompi_session_get_pset_data_nb_f(MPI_Fint *session, char *coll_pset_name, char *pset_name,
                                     char *keys, MPI_Fint *nkeys, MPI_Fint *wait,
                                     MPI_Fint *info_used, MPI_Fint *request, MPI_Fint *ierr,
                                     int coll_pset_name_len, int pset_name_len, int keys_strlen)
{
    MPI_Session c_session;
    char *c_coll_pset_name;
    char *c_pset_name;
    char **c_keys;
    int c_nkeys;
    int c_wait;
    MPI_Info c_info_used;
    MPI_Request c_request;
    int c_ierr = 0;

    c_session = PMPI_Session_f2c(*session);

    c_nkeys = OMPI_FINT_2_INT(*nkeys);
    c_wait = OMPI_FINT_2_INT(*wait);

    if (MPI_PARAM_CHECK) {
        if (NULL == c_session || MPI_SESSION_NULL == c_session) {
            if (NULL != ierr) {
                *ierr = OMPI_INT_2_FINT(MPI_ERR_ARG);
            }
        }
    }

    // convert strings
    if (OMPI_SUCCESS
        != (c_ierr = ompi_fortran_string_f2c(coll_pset_name, coll_pset_name_len,
                                             &c_coll_pset_name))) {
        if (NULL != ierr) {
            *ierr = OMPI_INT_2_FINT(c_ierr);
        }
        return;
    }

    if (OMPI_SUCCESS
        != (c_ierr = ompi_fortran_string_f2c(pset_name, pset_name_len, &c_pset_name))) {
        if (NULL != ierr) {
            *ierr = OMPI_INT_2_FINT(c_ierr);
        }
        return;
    }

    // convert keys into a c string array
    c_ierr = ompi_fortran_argv_count_f2c(keys, c_nkeys, keys_strlen, keys_strlen, &c_keys);
    if (0 != c_ierr) {
        if (NULL != ierr) {
            *ierr = OMPI_INT_2_FINT(c_ierr);
        }
        return;
    }

    c_ierr = ompi_instance_get_pset_data_nb((ompi_instance_t *) c_session, c_coll_pset_name,
                                         c_pset_name, c_keys, c_nkeys, c_wait,
                                         (opal_info_t **) &c_info_used, (ompi_request_t **) c_request);

    // return value
    if (NULL != ierr) {
        *ierr = OMPI_INT_2_FINT(c_ierr);
    }

    if (0 == c_ierr) {
        // output variables
        // TODO: does it make sense to return info_used here?
        *info_used = PMPI_Info_c2f(c_info_used);
        *request = PMPI_Request_c2f(c_request);
    }

    // free memory
    opal_argv_free(c_keys);
    free(c_coll_pset_name);
    free(c_pset_name);
}
