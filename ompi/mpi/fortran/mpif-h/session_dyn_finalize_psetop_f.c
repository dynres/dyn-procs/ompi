/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2004-2005 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2004-2005 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2004-2005 High Performance Computing Center Stuttgart,
 *                         University of Stuttgart.  All rights reserved.
 * Copyright (c) 2004-2005 The Regents of the University of California.
 *                         All rights reserved.
 * Copyright (c) 2011-2013 Cisco Systems, Inc.  All rights reserved.
 * Copyright (c) 2013      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * Copyright (c) 2015      Research Organization for Information Science
 *                         and Technology (RIST). All rights reserved.
 * Copyright (c) 2019      Triad National Security, LLC.  All rights reserved.
 *
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi_config.h"

#include "ompi/mpi/fortran/mpif-h/bindings.h"
#include "ompi/mpi/fortran/base/fortran_base_strings.h"
#include "ompi/constants.h"
#include "ompi/instance/instance.h"


#if OMPI_BUILD_MPI_PROFILING
#if OPAL_HAVE_WEAK_SYMBOLS
#pragma weak PMPI_SESSION_DYN_FINALIZE_PSETOP = ompi_session_dyn_finalize_psetop_f
#pragma weak pmpi_session_dyn_finalize_psetop = ompi_session_dyn_finalize_psetop_f
#pragma weak pmpi_session_dyn_finalize_psetop_ = ompi_session_dyn_finalize_psetop_f
#pragma weak pmpi_session_dyn_finalize_psetop__ = ompi_session_dyn_finalize_psetop_f

#pragma weak PMPI_Session_dyn_finalize_psetop_f = ompi_session_dyn_finalize_psetop_f
#pragma weak PMPI_Session_dyn_finalize_psetop_f08 = ompi_session_dyn_finalize_psetop_f
#else
OMPI_GENERATE_F77_BINDINGS (PMPI_SESSION_DYN_FINALIZE_PSETOP,
                            pmpi_session_dyn_finalize_psetop,
                            pmpi_session_dyn_finalize_psetop_,
                            pmpi_session_dyn_finalize_psetop__,
                            pompi_session_dyn_finalize_psetop_f,
                            (MPI_Fint *session, char *pset_name, MPI_Fint *ierr, int pset_name_len),
                            (session, pset_name, ierr, pset_name_len))
#endif
#endif

#if OPAL_HAVE_WEAK_SYMBOLS
#pragma weak MPI_SESSION_DYN_FINALIZE_PSETOP = ompi_session_dyn_finalize_psetop_f
#pragma weak mpi_session_dyn_finalize_psetop = ompi_session_dyn_finalize_psetop_f
#pragma weak mpi_session_dyn_finalize_psetop_ = ompi_session_dyn_finalize_psetop_f
#pragma weak mpi_session_dyn_finalize_psetop__ = ompi_session_dyn_finalize_psetop_f

#pragma weak MPI_Session_dyn_finalize_psetop_f = ompi_session_dyn_finalize_psetop_f
#pragma weak MPI_Session_dyn_finalize_psetop_f08 = ompi_session_dyn_finalize_psetop_f
#else
#if ! OMPI_BUILD_MPI_PROFILING
OMPI_GENERATE_F77_BINDINGS (MPI_SESSION_DYN_FINALIZE_PSETOP,
                            mpi_session_dyn_finalize_psetop,
                            mpi_session_dyn_finalize_psetop_,
                            mpi_session_dyn_finalize_psetop__,
                            ompi_session_dyn_finalize_psetop_f,
                            (MPI_Fint *session, char *pset_name, MPI_Fint *ierr, int pset_name_len),
                            (session, pset_name, ierr, pset_name_len))
#else
#define ompi_session_dyn_finalize_psetop_f pompi_session_dyn_finalize_psetop_f
#endif
#endif

// TODO: errhandler
void ompi_session_dyn_finalize_psetop_f(MPI_Fint *session, char *pset_name, MPI_Fint *ierr, int pset_name_len)
{
    MPI_Session c_session;
    int c_ierr = 0;
    char *c_pset_name;

    c_session = PMPI_Session_f2c(*session);

    // PARAM CHECK
    if (NULL == c_session || MPI_SESSION_NULL == c_session) {
        if (NULL != ierr) {
            *ierr = OMPI_INT_2_FINT(MPI_ERR_ARG);
        }
        return;
    }

    // convert pset_name
    if (OMPI_SUCCESS != (c_ierr = ompi_fortran_string_f2c(pset_name, pset_name_len,
                                                       &c_pset_name))) {
        if (NULL != ierr) {
            *ierr = OMPI_INT_2_FINT(c_ierr);
        }
        return;
    }

    c_ierr = ompi_instance_dyn_finalize_psetop((ompi_instance_t *) session, c_pset_name);

    if (NULL != ierr) {
        *ierr = OMPI_INT_2_FINT(c_ierr);
    }

    free(c_pset_name);
}
