/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2004-2005 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2004-2005 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2004-2005 High Performance Computing Center Stuttgart,
 *                         University of Stuttgart.  All rights reserved.
 * Copyright (c) 2004-2005 The Regents of the University of California.
 *                         All rights reserved.
 * Copyright (c) 2011-2013 Cisco Systems, Inc.  All rights reserved.
 * Copyright (c) 2013      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * Copyright (c) 2015      Research Organization for Information Science
 *                         and Technology (RIST). All rights reserved.
 * Copyright (c) 2019      Triad National Security, LLC.  All rights reserved.
 *
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi_config.h"

#include "ompi/mpi/fortran/mpif-h/bindings.h"
#include "ompi/mpi/fortran/base/fortran_base_strings.h"
#include "ompi/constants.h"
#include "ompi/instance/instance.h"
#include "opal/util/argv.h"


#if OMPI_BUILD_MPI_PROFILING
#if OPAL_HAVE_WEAK_SYMBOLS
#pragma weak PMPI_SESSION_PSET_BARRIER = ompi_session_pset_barrier_f
#pragma weak pmpi_session_pset_barrier = ompi_session_pset_barrier_f
#pragma weak pmpi_session_pset_barrier_ = ompi_session_pset_barrier_f
#pragma weak pmpi_session_pset_barrier__ = ompi_session_pset_barrier_f

#pragma weak PMPI_Session_pset_barrier_f = ompi_session_pset_barrier_f
#pragma weak PMPI_Session_pset_barrier_f08 = ompi_session_pset_barrier_f
#else
OMPI_GENERATE_F77_BINDINGS (PMPI_SESSION_PSET_BARRIER,
                            pmpi_session_pset_barrier,
                            pmpi_session_pset_barrier_,
                            pmpi_session_pset_barrier__,
                            pompi_session_pset_barrier_f,
                            (MPI_Fint *session, char *psets, MPI_Fint *npsets, MPI_Fint *info, MPI_Fint *ierr, int psets_strlen),
                            (session, psets, npsets, info, ierr, psets_strlen))
#endif
#endif

#if OPAL_HAVE_WEAK_SYMBOLS
#pragma weak MPI_SESSION_PSET_BARRIER = ompi_session_pset_barrier_f
#pragma weak mpi_session_pset_barrier = ompi_session_pset_barrier_f
#pragma weak mpi_session_pset_barrier_ = ompi_session_pset_barrier_f
#pragma weak mpi_session_pset_barrier__ = ompi_session_pset_barrier_f

#pragma weak MPI_Session_pset_barrier_f = ompi_session_pset_barrier_f
#pragma weak MPI_Session_pset_barrier_f08 = ompi_session_pset_barrier_f
#else
#if ! OMPI_BUILD_MPI_PROFILING
OMPI_GENERATE_F77_BINDINGS (MPI_SESSION_PSET_BARRIER,
                            mpi_session_pset_barrier,
                            mpi_session_pset_barrier_,
                            mpi_session_pset_barrier__,
                            ompi_session_pset_barrier_f,
                            (MPI_Fint *session, char *psets, MPI_Fint *npsets, MPI_Fint *info, MPI_Fint *ierr, int psets_strlen),
                            (session, psets, npsets, info, ierr, psets_strlen))
#else
#define ompi_session_pset_barrier_f pompi_session_pset_barrier_f
#endif
#endif

/*
** For FORTRAN, we are adjusting the API a bit:
** output_sets needs to be a pre-allocated array of strings by the caller
** noutput must be the length of the array (i.e. the number of strings)
*/
// TODO: errhandler
void ompi_session_pset_barrier_f(MPI_Fint *session, char *psets, MPI_Fint *npsets, MPI_Fint *info, MPI_Fint *ierr, int psets_strlen)
{
    MPI_Session c_session;
    char **c_psets;
    int c_npsets;
    MPI_Info c_info;

    int c_ierr = 0;

    c_session = PMPI_Session_f2c(*session);

    // PARAM CHECK
    if (MPI_PARAM_CHECK) {
        if (NULL == c_session || MPI_SESSION_NULL == c_session) {
            if (NULL != ierr) {
                *ierr = OMPI_INT_2_FINT(MPI_ERR_ARG);
            }
            return;
        }
    }

    c_npsets = OMPI_FINT_2_INT(*npsets);

    // convert input_sets into a c string array
    c_ierr = ompi_fortran_argv_count_f2c(psets, c_npsets, psets_strlen, psets_strlen,
                                         &c_psets);
    if (0 != c_ierr) {
        if (NULL != ierr) {
            *ierr = OMPI_INT_2_FINT(c_ierr);
        }
        return;
    }

    c_info = PMPI_Info_f2c(*info);

    c_ierr = ompi_instance_pset_barrier(c_psets, c_npsets, (ompi_info_t *) c_info);

    if (NULL != ierr) {
        *ierr = OMPI_INT_2_FINT(c_ierr);
    }
    // free memory
    opal_argv_free(c_psets);
}
