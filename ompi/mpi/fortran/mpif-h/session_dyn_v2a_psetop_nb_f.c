/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2004-2005 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2004-2005 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2004-2005 High Performance Computing Center Stuttgart,
 *                         University of Stuttgart.  All rights reserved.
 * Copyright (c) 2004-2005 The Regents of the University of California.
 *                         All rights reserved.
 * Copyright (c) 2011-2013 Cisco Systems, Inc.  All rights reserved.
 * Copyright (c) 2013      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * Copyright (c) 2015      Research Organization for Information Science
 *                         and Technology (RIST). All rights reserved.
 * Copyright (c) 2019      Triad National Security, LLC.  All rights reserved.
 *
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi_config.h"

#include "ompi/mpi/fortran/mpif-h/bindings.h"
#include "ompi/mpi/fortran/base/fortran_base_strings.h"
#include "ompi/constants.h"
#include "ompi/instance/instance.h"
#include "opal/util/argv.h"


#if OMPI_BUILD_MPI_PROFILING
#if OPAL_HAVE_WEAK_SYMBOLS
#pragma weak PMPI_SESSION_DYN_V2A_PSETOP_NB = ompi_session_dyn_v2a_psetop_nb_f
#pragma weak pmpi_session_dyn_v2a_psetop_nb = ompi_session_dyn_v2a_psetop_nb_f
#pragma weak pmpi_session_dyn_v2a_psetop_nb_ = ompi_session_dyn_v2a_psetop_nb_f
#pragma weak pmpi_session_dyn_v2a_psetop_nb__ = ompi_session_dyn_v2a_psetop_nb_f

#pragma weak PMPI_Session_dyn_v2a_psetop_nb_f = ompi_session_dyn_v2a_psetop_nb_f
#pragma weak PMPI_Session_dyn_v2a_psetop_nb_f08 = ompi_session_dyn_v2a_psetop_nb_f
#else
OMPI_GENERATE_F77_BINDINGS (PMPI_SESSION_DYN_V2A_PSETOP_NB,
                            pmpi_session_dyn_v2a_psetop_nb,
                            pmpi_session_dyn_v2a_psetop_nb_,
                            pmpi_session_dyn_v2a_psetop_nb__,
                            pompi_session_dyn_v2a_psetop_nb_f,
                            (MPI_Fint *session, MPI_Fint *op, char *input_sets, MPI_Fint *ninput, char ***output_sets, int *noutput, MPI_Fint *info, MPI_Fint *request, MPI_Fint *ierr, int input_sets_strlen, int output_sets_strlen),
                            (session, op, input_sets, ninput, output_sets, noutput, info, request, ierr, input_sets_strlen))
#endif
#endif

#if OPAL_HAVE_WEAK_SYMBOLS
#pragma weak MPI_SESSION_DYN_V2A_PSETOP_NB = ompi_session_dyn_v2a_psetop_nb_f
#pragma weak mpi_session_dyn_v2a_psetop_nb = ompi_session_dyn_v2a_psetop_nb_f
#pragma weak mpi_session_dyn_v2a_psetop_nb_ = ompi_session_dyn_v2a_psetop_nb_f
#pragma weak mpi_session_dyn_v2a_psetop_nb__ = ompi_session_dyn_v2a_psetop_nb_f

#pragma weak MPI_Session_dyn_v2a_psetop_nb_f = ompi_session_dyn_v2a_psetop_nb_f
#pragma weak MPI_Session_dyn_v2a_psetop_nb_f08 = ompi_session_dyn_v2a_psetop_nb_f
#else
#if ! OMPI_BUILD_MPI_PROFILING
OMPI_GENERATE_F77_BINDINGS (MPI_SESSION_DYN_V2A_PSETOP_NB,
                            mpi_session_dyn_v2a_psetop_nb,
                            mpi_session_dyn_v2a_psetop_nb_,
                            mpi_session_dyn_v2a_psetop_nb__,
                            ompi_session_dyn_v2a_psetop_nb_f,
                            (MPI_Fint *session, MPI_Fint *op, char *input_sets, MPI_Fint *ninput, char ***output_sets, int *noutput, MPI_Fint *info, MPI_Fint *request, MPI_Fint *ierr, int input_sets_strlen, int output_sets_strlen),
                            (session, op, input_sets, ninput, output_sets, noutput, info, request, ierr, input_sets_strlen, output_sets_strlen))
#else
#define ompi_session_dyn_v2a_psetop_nb_f pompi_session_dyn_v2a_psetop_nb_f
#endif
#endif

void ompi_session_dyn_v2a_psetop_nb_f (MPI_Fint *session, MPI_Fint *op, char *input_sets, MPI_Fint *ninput, char ***output_sets, int *noutput, MPI_Fint *info, MPI_Fint *request, MPI_Fint *ierr, int input_sets_strlen, int output_sets_strlen)
{
    MPI_Session c_session;
    int c_op;
    char **c_input_sets;
    int c_ninput;
    int c_noutput;
    MPI_Request c_request;
    MPI_Info c_info;

    printf("\n-----------------------------------------------------------------------------------------------------------\n");
    printf("\n-------------------------------IN NONNNNNN BLOCKING OMPI CALL----------------------------------------------\n");
    printf("\n-----------------------------------------------------------------------------------------------------------\n");

    int c_ierr = 0;
    c_session = PMPI_Session_f2c(*session);

    // PARAM CHECK
    if (MPI_PARAM_CHECK) {
        if (NULL == c_session || MPI_SESSION_NULL == c_session) {
            if (NULL != ierr) {
                *ierr = OMPI_INT_2_FINT(MPI_ERR_ARG);
            }
            return;
        }
    }

    c_op = OMPI_FINT_2_INT(*op);
    c_ninput = OMPI_FINT_2_INT(*ninput);

    // convert input_sets into a c string array
    c_ierr = ompi_fortran_argv_count_f2c(input_sets, c_ninput, input_sets_strlen, input_sets_strlen,
                                         &c_input_sets);

    
   // for (int i = 0; i < c_ninput; i++) { 
   //     printf("C input sets -- %s\n", c_input_sets[i]);
   // }
   // printf("pmpi_request_c2f\n");
    if (0 != c_ierr) {
        if (NULL != ierr) {
            *ierr = OMPI_INT_2_FINT(c_ierr);
        }
        return;
    }
   
    c_info = PMPI_Info_f2c(*info);
    c_ierr = ompi_instance_dyn_v2a_pset_op_nb((ompi_instance_t *) c_session, &c_op, c_input_sets,
                                          c_ninput, output_sets, (int *) noutput,
                                          (ompi_info_t *) c_info, (ompi_request_t **) &c_request);

    //MPI_Wait(&c_request, MPI_STATUS_IGNORE);
    //*output_sets = c_output_psets;

    //for (int i = 0; i < c_noutput; i++) { 
    //    printf("c output sets--- %d: %s\n", i,  c_output_psets[i]); 
    //} 

    //for (int i = 0; i < c_noutput; i++) { 
    //    printf("output sets--- %d: %s\n", i,  (*output_sets)[i]); 
    //} 

    if (MPI_SUCCESS == c_ierr) {
      *request = PMPI_Request_c2f(c_request);
    }

   

    if(c_request == MPI_REQUEST_NULL) {
        printf("MPI REQUEST NULL/INACTIVE!");
    }
    //printf("fortran mpi request: %f\n", *request);
    //printf("c_noutput: %d\n", c_noutput); 
    //printf("c_op: %d\n", c_op); 
    //printf("c_noutput: %d\n", c_noutput);

    if (NULL != ierr) {
        *ierr = OMPI_INT_2_FINT(c_ierr);
    }

    // free memory
    opal_argv_free(c_input_sets);
}
