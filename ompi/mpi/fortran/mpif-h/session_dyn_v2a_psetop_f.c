/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2004-2005 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2004-2005 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2004-2005 High Performance Computing Center Stuttgart,
 *                         University of Stuttgart.  All rights reserved.
 * Copyright (c) 2004-2005 The Regents of the University of California.
 *                         All rights reserved.
 * Copyright (c) 2011-2013 Cisco Systems, Inc.  All rights reserved.
 * Copyright (c) 2013      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * Copyright (c) 2015      Research Organization for Information Science
 *                         and Technology (RIST). All rights reserved.
 * Copyright (c) 2019      Triad National Security, LLC.  All rights reserved.
 *
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi_config.h"

#include "ompi/mpi/fortran/mpif-h/bindings.h"
#include "ompi/mpi/fortran/base/fortran_base_strings.h"
#include "ompi/constants.h"
#include "ompi/instance/instance.h"
#include "opal/util/argv.h"


#if OMPI_BUILD_MPI_PROFILING
#if OPAL_HAVE_WEAK_SYMBOLS
#pragma weak PMPI_SESSION_DYN_V2A_PSETOP = ompi_session_dyn_v2a_psetop_f
#pragma weak pmpi_session_dyn_v2a_psetop = ompi_session_dyn_v2a_psetop_f
#pragma weak pmpi_session_dyn_v2a_psetop_ = ompi_session_dyn_v2a_psetop_f
#pragma weak pmpi_session_dyn_v2a_psetop__ = ompi_session_dyn_v2a_psetop_f

#pragma weak PMPI_Session_dyn_v2a_psetop_f = ompi_session_dyn_v2a_psetop_f
#pragma weak PMPI_Session_dyn_v2a_psetop_f08 = ompi_session_dyn_v2a_psetop_f
#else
OMPI_GENERATE_F77_BINDINGS (PMPI_SESSION_DYN_V2A_PSETOP,
                            pmpi_session_dyn_v2a_psetop,
                            pmpi_session_dyn_v2a_psetop_,
                            pmpi_session_dyn_v2a_psetop__,
                            pompi_session_dyn_v2a_psetop_f,
                            (MPI_Fint *session, MPI_Fint *op, char *input_sets, MPI_Fint *ninput, char *output_sets, MPI_Fint *noutput, MPI_Fint *info, MPI_Fint *ierr, int input_sets_strlen),
                            (session, op, input_sets, ninput, output_sets, noutput, info, ierr, input_sets_strlen, output_sets_strlen))
#endif
#endif

#if OPAL_HAVE_WEAK_SYMBOLS
#pragma weak MPI_SESSION_DYN_V2A_PSETOP = ompi_session_dyn_v2a_psetop_f
#pragma weak mpi_session_dyn_v2a_psetop = ompi_session_dyn_v2a_psetop_f
#pragma weak mpi_session_dyn_v2a_psetop_ = ompi_session_dyn_v2a_psetop_f
#pragma weak mpi_session_dyn_v2a_psetop__ = ompi_session_dyn_v2a_psetop_f

#pragma weak MPI_Session_dyn_v2a_psetop_f = ompi_session_dyn_v2a_psetop_f
#pragma weak MPI_Session_dyn_v2a_psetop_f08 = ompi_session_dyn_v2a_psetop_f
#else
#if ! OMPI_BUILD_MPI_PROFILING
OMPI_GENERATE_F77_BINDINGS (MPI_SESSION_DYN_V2A_PSETOP,
                            mpi_session_dyn_v2a_psetop,
                            mpi_session_dyn_v2a_psetop_,
                            mpi_session_dyn_v2a_psetop__,
                            ompi_session_dyn_v2a_psetop_f,
                            (MPI_Fint *session, MPI_Fint *op, char *input_sets, MPI_Fint *ninput, char *output_sets, MPI_Fint *noutput, MPI_Fint *info, MPI_Fint *ierr, int input_sets_strlen, int output_sets_strlen),
                            (session, op, input_sets, ninput, output_sets, noutput, info, ierr, input_sets_strlen, output_sets_strlen))
#else
#define ompi_session_dyn_v2a_psetop_f pompi_session_dyn_v2a_psetop_f
#endif
#endif

/*
** For FORTRAN, we are adjusting the API a bit:
** output_sets needs to be a pre-allocated array of strings by the caller
** noutput must be the length of the array (i.e. the number of strings)
*/
// TODO: errhandler
void ompi_session_dyn_v2a_psetop_f(MPI_Fint *session, MPI_Fint *op, char *input_sets,
                                   MPI_Fint *ninput, char *output_sets, MPI_Fint *noutput,
                                   MPI_Fint *info, MPI_Fint *ierr, int input_sets_strlen, int output_sets_strlen)
{
    MPI_Session c_session;
    int c_op;
    char **c_input_sets;
    int c_ninput;
    char **c_output_sets = NULL;
    int c_noutput;
    int c_noutput_orig;
    MPI_Info c_info;

    int c_ierr = 0;

    c_session = PMPI_Session_f2c(*session);

    // PARAM CHECK
    if (MPI_PARAM_CHECK) {
        if (NULL == c_session || MPI_SESSION_NULL == c_session) {
            if (NULL != ierr) {
                *ierr = OMPI_INT_2_FINT(MPI_ERR_ARG);
            }
            return;
        }
    }

    c_op = OMPI_FINT_2_INT(*op);

    c_ninput = OMPI_FINT_2_INT(*ninput);

    c_noutput_orig = OMPI_FINT_2_INT(*noutput);

    // convert input_sets into a c string array
    c_ierr = ompi_fortran_argv_count_f2c(input_sets, c_ninput, input_sets_strlen, input_sets_strlen,
                                         &c_input_sets);
    if (0 != c_ierr) {
        if (NULL != ierr) {
            *ierr = OMPI_INT_2_FINT(c_ierr);
        }
        return;
    }

    c_info = PMPI_Info_f2c(*info);

    c_noutput = 0;
    c_ierr = ompi_instance_dyn_v2a_pset_op((ompi_instance_t *) c_session, &c_op, c_input_sets,
                                          c_ninput, &c_output_sets, &c_noutput,
                                          (ompi_info_t *) c_info);

    /* printf("c_noutput: %d\n", c_noutput); */
    /* for (int i = 0; i < c_noutput; i++) { */
    /*     printf("--- %d: %s\n", i, c_output_sets[i]); */
    /* } */
    /* printf("c_op: %d\n", c_op); */

    if (NULL != ierr) {
        *ierr = OMPI_INT_2_FINT(c_ierr);
    }

    /* FATAL ERROR! */
    if (c_noutput > c_noutput_orig) {
        if (NULL != ierr) {
            *ierr = OMPI_INT_2_FINT(MPI_ERR_ARG);
        }
        opal_argv_free(c_input_sets);

        // free output_sets
        for (int i = 0; i < c_noutput; i++) {
            free(c_output_sets[i]);
        }
        free(c_output_sets);
        return;
    }

    if (0 == c_ierr) {
        // return variables
        *op = OMPI_FINT_2_INT(c_op);
        *noutput = OMPI_FINT_2_INT(c_noutput);
        *info = PMPI_Info_c2f(c_info);
        ompi_fortran_string_array_c2f(c_output_sets, c_noutput, output_sets, output_sets_strlen);

        // free output_sets
        for (int i = 0; i < c_noutput; i++) {
            free(c_output_sets[i]);
        }
        free(c_output_sets);
    }

    // free memory
    opal_argv_free(c_input_sets);
}
