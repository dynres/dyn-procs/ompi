/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2004-2005 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2004-2005 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2004-2005 High Performance Computing Center Stuttgart,
 *                         University of Stuttgart.  All rights reserved.
 * Copyright (c) 2004-2005 The Regents of the University of California.
 *                         All rights reserved.
 * Copyright (c) 2011-2013 Cisco Systems, Inc.  All rights reserved.
 * Copyright (c) 2013      Los Alamos National Security, LLC. All rights
 *                         reserved.
 * Copyright (c) 2015      Research Organization for Information Science
 *                         and Technology (RIST). All rights reserved.
 * Copyright (c) 2019      Triad National Security, LLC.  All rights reserved.
 *
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi_config.h"

#include "ompi/mpi/fortran/mpif-h/bindings.h"
#include "ompi/mpi/fortran/base/fortran_base_strings.h"
#include "ompi/constants.h"
#include "ompi/instance/instance.h"


#if OMPI_BUILD_MPI_PROFILING
#if OPAL_HAVE_WEAK_SYMBOLS
#pragma weak PMPI_SESSION_DYN_V2A_QUERY_PSETOP = ompi_session_dyn_v2a_query_psetop_f
#pragma weak pmpi_session_dyn_v2a_query_psetop = ompi_session_dyn_v2a_query_psetop_f
#pragma weak pmpi_session_dyn_v2a_query_psetop_ = ompi_session_dyn_v2a_query_psetop_f
#pragma weak pmpi_session_dyn_v2a_query_psetop__ = ompi_session_dyn_v2a_query_psetop_f

#pragma weak PMPI_Session_dyn_v2a_query_psetop_f = ompi_session_dyn_v2a_query_psetop_f
#pragma weak PMPI_Session_dyn_v2a_query_psetop_f08 = ompi_session_dyn_v2a_query_psetop_f
#else
OMPI_GENERATE_F77_BINDINGS (PMPI_SESSION_DYN_V2A_QUERY_PSETOP,
                            pmpi_session_dyn_v2a_query_psetop,
                            pmpi_session_dyn_v2a_query_psetop_,
                            pmpi_session_dyn_v2a_query_psetop__,
                            pompi_session_dyn_v2a_query_psetop_f,
                            (MPI_Fint *session, char *coll_pset, char *input_pset, MPI_Fint *type, char ***output_psets, MPI_Fint *noutput, MPI_Fint *ierr, int coll_pset_len, int input_pset_len, int output_psets_strlen),
                            (session, coll_pset, input_pset, type, output_pset, noutput, ierr, coll_pset_len, input_pset_len, output_psets_strlen))
#endif
#endif

#if OPAL_HAVE_WEAK_SYMBOLS
#pragma weak MPI_SESSION_DYN_V2A_QUERY_PSETOP = ompi_session_dyn_v2a_query_psetop_f
#pragma weak mpi_session_dyn_v2a_query_psetop = ompi_session_dyn_v2a_query_psetop_f
#pragma weak mpi_session_dyn_v2a_query_psetop_ = ompi_session_dyn_v2a_query_psetop_f
#pragma weak mpi_session_dyn_v2a_query_psetop__ = ompi_session_dyn_v2a_query_psetop_f

#pragma weak MPI_Session_dyn_v2a_query_psetop_f = ompi_session_dyn_v2a_query_psetop_f
#pragma weak MPI_Session_dyn_v2a_query_psetop_f08 = ompi_session_dyn_v2a_query_psetop_f
#else
#if ! OMPI_BUILD_MPI_PROFILING
OMPI_GENERATE_F77_BINDINGS (MPI_SESSION_DYN_V2A_QUERY_PSETOP,
                            mpi_session_dyn_v2a_query_psetop,
                            mpi_session_dyn_v2a_query_psetop_,
                            mpi_session_dyn_v2a_query_psetop__,
                            ompi_session_dyn_v2a_query_psetop_f,
                            (MPI_Fint *session, char *coll_pset, char *input_pset, MPI_Fint *type, char *output_psets, MPI_Fint *noutput, MPI_Fint *ierr, int coll_pset_len, int input_pset_len, int output_psets_strlen),
                            (session, coll_pset, input_pset, type, output_pset, noutput, ierr, coll_pset_len, input_pset_len, output_psets_strlen))
#else
#define ompi_session_dyn_v2a_query_psetop_f pompi_session_dyn_v2a_query_psetop_f
#endif
#endif

// we assume noutput and output_psets to work as in v2a_psetop
// TODO: errhandler
void ompi_session_dyn_v2a_query_psetop_f(MPI_Fint *session, char *coll_pset, char *input_pset,
                                         MPI_Fint *type, char *output_psets, MPI_Fint *noutput,
                                         MPI_Fint *ierr, int coll_pset_len, int input_pset_len,
                                         int output_psets_strlen)
{
    MPI_Session c_session;
    char *c_coll_pset;
    char *c_input_pset;
    int c_type;
    ompi_psetop_type_t ompi_rc_op_type = OMPI_PSETOP_NULL;
    char **c_output_psets = NULL;
    size_t c_noutput_s;
    int c_noutput;
    int c_noutput_orig;

    int c_ierr = 0;

    c_session = PMPI_Session_f2c(*session);

    // PARAM CHECK
    if (MPI_PARAM_CHECK) {
        if (NULL == c_session || MPI_SESSION_NULL == c_session) {
            if (NULL != ierr) {
                *ierr = OMPI_INT_2_FINT(MPI_ERR_ARG);
            }
            return;
        }
    }

    c_noutput_orig = OMPI_FINT_2_INT(*noutput);

    if (OMPI_SUCCESS != (c_ierr = ompi_fortran_string_f2c(coll_pset, coll_pset_len,
                                                       &c_coll_pset))) {
        if (NULL != ierr) {
            *ierr = OMPI_INT_2_FINT(c_ierr);
        }
        return;
    }

    if (OMPI_SUCCESS != (c_ierr = ompi_fortran_string_f2c(input_pset, input_pset_len,
                                                       &c_input_pset))) {
        if (NULL != ierr) {
            *ierr = OMPI_INT_2_FINT(c_ierr);
        }
        return;
    }

    c_noutput_s = 0;
    c_ierr = ompi_instance_dyn_v2a_query_psetop((ompi_instance_t *) c_session, c_coll_pset, c_input_pset, &ompi_rc_op_type, &c_output_psets, &c_noutput_s, false);

    c_noutput = (int)c_noutput_s;

    if (NULL != ierr) {
        *ierr = OMPI_INT_2_FINT(c_ierr);
    }

    free(c_coll_pset);
    free(c_input_pset);

    /* FATAL ERROR! */
    if (c_noutput > c_noutput_orig) {
        if (NULL != ierr) {
            *ierr = OMPI_INT_2_FINT(MPI_ERR_ARG);
        }

        // free output_psets
        for (int i = 0; i < c_noutput; i++) {
            free(c_output_psets[i]);
        }
        free(c_output_psets);
        return;
    }

    if (0 == c_ierr) {
        // return variables
        c_type = MPI_OMPI_CONVT_PSET_OP(ompi_rc_op_type);
        *type = OMPI_FINT_2_INT(c_type);
        *noutput = OMPI_FINT_2_INT(c_noutput);
        ompi_fortran_string_array_c2f(c_output_psets, c_noutput, output_psets, output_psets_strlen);

        // free output_psets
        for (int i = 0; i < c_noutput; i++) {
            free(c_output_psets[i]);
        }
        free(c_output_psets);
    }
}
