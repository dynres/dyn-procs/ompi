# Time-X EuroHPC project: MPI Extensions for Dynamic Resources (based on Open MPI)

This project is a fork of the Open-MPI repository and provides an extended MPI Sessions interface for dynamic resources.
The goal of the project is to provide a flexible MPI interface to write MPI programs with dynamic resources.
The current version is an early prototype and will be updated/extended on a regular basis.

## Installation
This version of OpenMPI depends on the following custom versions of OpenPMIx and PRRTE:
<ul>
  <li>[OpenPMIx](https://gitlab.inria.fr/dynres/dyn-procs/openpmix) </li>
  <li>[PRRTE](https://gitlab.inria.fr/dynres/dyn-procs/prrte) </li>
</ul> 

<p> To setup OpenMPI with these versions of OpenPMIx and PRRTE it is recommended to use the setup description and scripts provided in the following repository:<br>
https://gitlab.inria.fr/dynres/dyn-procs/dyn_procs_setup <br>
<br> 
To manually set up Open-MPI, once you have build the custom OpenPMIx and PRRTE versions, run the following commands:

1. Generate the configure scripts:
`./autogen.pl`

2. Run the configure script: 
```./configure --prefix=/path/for/ompi/install --with-pmix=/path/to/custom/pmix --with-prrte=/path/to/custom/prrte```

3. Run make: 
```make -j all install``` 
 
Also see the HACKING.md file for more information on building and installing MPI.

## Dynamic MPI Interface
This project extends the MPI Sessions Interface (introduced in the MPI Standard 4.0) with additional routines for dynamic resources.
These routines introduce two new features: 
 <ol>
  <li>Process Set Dictionaries</li>
  <li>Set Operations</li>
</ol> 

IMPORTANT: The current interface is still under development and will be updated on a regular basis. It is mainly intended for experimentation. 

### Process Set Dictionaries
Each process set has an associated dictionary which allows MPI processes to publish and lookup data associated with the process set.
A main use case for this is to provide dynamically added processes with kick-off information required to e.g. setup communicators with existing processes.
We provide the routines `MPI_Session_set_pset_data` and `MPI_Session_get_pset_data` to publish/lookup key-values in the dictionary:

```
int MPI_Session_set_pset_data(
      MPI_Session session,    // session handle
      const char* pset_name,  // Name of PSet in whose Dictionary the data should be published
      MPI_Info info           // Info object containg key-value pairs to be published
)
RETURN: 
   - MPI_SUCCESS if operation was successful

Description:   Publishes a key-value pair in the dictionary associated with the given PSet name.
               The PSet name has to exist.

``` 

```
int MPI_Session_get_pset_data(_nb)(
      MPI_Session session,    // session handle
      char *coll_pset_name,   /* Name of a PSet specifying procs of the collective
 	                         * → for consensus finding without primary process
 	                         * → all processes receive the same value(s)
 	                         */  
      char *pset_name,        // Name of PSet in whose Dictionary the data should be looked up
      char **keys,            // The keys for which the values should be looked up
      int nkeys,              // The number of keys
      int wait,               // Flag indicating wether to wait for the values to be published
      MPI_Info *info_used     // OUT: MPI_Info object containing the requested key-values
      (MPI_Request *request)  // OUT: An MPI Request for the non-blocking version
)
RETURN: 
   - MPI_SUCCESS if operation was successful

Description:   Looks up a key-value pair in the dictionary associated with the given PSet name.
               The PSet name has to exist.
               The call is collective over the processes in coll_pset_name, i.e. all processes in 
               coll_pset_name have to call this function. All processes are guaranteed to receive the
               same values. mpi://SELF may be used for individual lookups.
``` 
### Set Operations
PSet Operations are executed on a number of input PSets to produce a number of output PSets.
This might involve adding new processes to the application or removing processes from the application.
We provide the three routines `MPI_Session_dyn_v2a_psetop(_nb)`, `MPI_Session_dyn_v2a_query_psetop(_nb)` and `MPI_Session_dyn_finalize_psetop`:

``` 
int MPI_Session_dyn_v2a_psetop(_nb)(
      MPI_Session session,    // session handle
      int *op                 // INOUT: The set operation to be executed (See list of operations below)
                              // (Will be set to MPI_PSETOP_NULL if requested resources not available)
      char **input_psets      // Names of the input Psets
      int ninput_psets        // Number of input Psets
      char ***output_psets    // OUT: Names of the resulting new Psets
      int *n_output_psets     // OUT: Number of resulting new Psets
      MPI_Info info           // Info object to further specify the operation
      (MPI_Request *request)  // OUT: An MPI Request for the non-blocking version
)
RETURN: 
   - MPI_SUCCESS if operation was successful

Description:   Requests the specified PSet Operation to be applied on the input PSets.
               The info object can be used to specify parameters of the operation.
               If successful, the function allocates an array of n_output PSet names.
               It is the callers responsibility to free the PSet names and the output_psets array.
``` 

``` 
int MPI_Session_dyn_v2a_query_psetop(_nb)(
      MPI_Session session,    // session handle
      char *coll_pset,        // Name of the collective Pset (similar to MPI_Session_get_pset_data) 
      char *pset_name,        // Name of the Pset for which the psetop should be query (one of the in/output psets) 
      int *op,                // OUT: The operation (ADD, SUB, GROW, SHRINK, REPLACE)		
      char ***output_psets,   // OUT: Array of output Pset of the operation 
      int *noutput_psets,     // OUT: Number of output psets
      (MPI_request *request)  // OUT: MPI_Request for the non-blocking version
)
RETURN: 
   - MPI_SUCCESS if operation was successful

Description:   Queries for pending PSet Operation invloving the specified PSet.
               This only applies to PSet operations involving changes of resources:
                  -> MPI_PSETOP_{ADD, SUB; GROW, SHRINK, REPLACE}
               If no pending PSet operation is found for the specified PSet, op will be set to MPI_PSETOP_NULL
``` 
``` 
int MPI_Session_dyn_finalize_psetop(
      MPI_Session session,    // session handle
      char *pset_name         // Name of the Pset for which the psetop should be finalized (one of the in/output sets)
)
RETURN: 
   - MPI_SUCCESS if operation was successful

Description:   Indicates finalization of the PSet Operation.
               This will make the operation unavailable for MPI_Session_dyn_v2a_query_psetop.
               This only applies to PSet operations involving changes of resources:
                  -> MPI_PSETOP_{ADD, SUB; GROW, SHRINK, REPLACE}
``` 


``` 
List of possible PSet operations:
      - MPI_PSETOP_NULL          noop. Used as output to indicate that the requested operation could not be executed.
      - MPI_PSETOP_ADD           Creates a new set of processes. 
                                 The number of new processes can be specified with the "mpi_num_procs_add" key in the info object.
      - MPI_PSETOP_SUB           Creates a set of processes to be removed, which is a subset of the input sets.
                                 The number of processes to be removed can be specified with the "mpi_num_procs_sub" key in the info object.
      - MPI_PSETOP_SPLIT         Creates non-overlapping subsets of the input PSet.
                                 The "mpi_part_sizes" key specifies the sizes of the subsets as a comma-separated list.
                                 E.g. S_in(64) could be split in 3 Sets of sizes 8, 24, 32 with "mpi_part_sizes" -> "8,24,32"
      - MPI_PSETOP_UNION         Creates the UNION PSet of the provided input PSets.
      - MPI_PSETOP_DIFFERENCE    Creates the DIFFERENCE PSet of the provided input PSets.
      - MPI_PSETOP_INTERSECTION  Creates the INTERSECTION PSET of the provided input PSets.
      - MPI_PSETOP_GROW          Creates a new set of processes S_new and the UNION PSet S_union = S_in UNION S_new.
                                 The number of new processes can be specified with the "mpi_num_procs_add" key in the info object.
      - MPI_PSETOP_SHRINK        Creates a set of processes S_remove to be removed and the DIFFRENCE PSet S_diff = S_in DIFF S_remove
                                 The number of new processes can be specified with the "mpi_num_procs_sub" key in the info object.
      - MPI_PSETOP_REPLACE       Creates 3 PSets:
                                    - S_new:    A new set of processes
                                    - S_del:    Processes to be removed
                                    - S_repl:   S_in UNION S_new DIFFERENCE S_del
                                 The keys "mpi_num_procs_add" and "mpi_num_procs_sub" specify the number of processes to be added/removed.
``` 
``` 
Additional PSet attributes available via MPI_Session_get_pset_info:
      "mpi_dyn"      ->    bool     // This PSet is the mpi://WORLD PSet of dynamically added processes, 
                                    // i.e. it was created by MPI_PSETOP_ADD or MPI_PSETOP_GROW
                                    // Use case: E.g. allows processes to determine if they where added dynamically
      "mpi_primary"  ->    bool     // True, if the calling process is the primary process of the PSet
                                    // Only one process is the primary process of a PSet
                                    // Use case: E.g. Have a "root" process without a communicator
      "mpi_included" ->    bool     // True, if the calling process is included in the PSet

``` 
## Launching
To launch dynamic MPI applications use the following command:
``` 
mpirun -np <number of processes to start> --host <hostlist specifying the total node pool> my_awesome_application
``` 
IMPORTANT: See the limitations below.

It is also possible to run multiple applications using the prte and prun commands.

## Limitations
<ul>
  <li>Currently, we only support node granularity of resource changes. 
  That is, when adding/removing processes it has to be multiples of the node slots.
  E.g. you can do something like this (here: [x,x,o] would be a node with 3 slots and 2 running processes): 
  <ul>
  
   <li>[x,x,x],[x,x,x]           ->    [x,x,x],[x,x,x],[x,x,x]</li>
   <li>[x,x,x],[x,x,x],[x,x,x]   ->    [x,x,x],[x,x,x]</li>
  </ul>
  But not this:
    <ul>
   <li>[x,x,x],[x,o,o]           ->    [x,x,x],[x,x,o]</li>
   <li>[x,x,x],[x,x,x]           ->    [x,x,x],[x,o,o]</li>
  </ul>
  </li>
  <li>Currently, we only support multithreading up to MPI_THREAD_FUNNELED </li>
  <li>Currently, the initial node pool (given with the --host option to mpirun/prte) is fixed, 
      i.e. resources can only be changed inside of this node pool </li>
</ul> 

## Examples
Some example applications are provided in the following repo:

https://gitlab.inria.fr/dynres/dyn-procs/test_applications


## Tutorial

In this tutorial we will develop a simple dynamic MPI application. 
It is recommended to be familiar with the MPI Session model, i.e. chapter 11 of the MPI Standard 4.0.

Our example application will start with 2 MPI processes and increase the number of MPI processes to 4.

An illustration of the program flow is given below:

  <ul>
   <li> The left column shows the program flow for the original processes {p1, p2} </li>
   <li> The middle column shows the currently defined PSets </li>
   <li> The right column shows the program flow for the dynamically added processes {p3, p4} </li>
  </ul>


```
___________________________________________________________________________________________________________________________________ 
|_______ Original MPI processes (p1, p2) _______|____________ PSets _____________|________ Dynamic MPI Processes (p3, p4)__________|
|                                               |                                |                                                 |
| ======== ENTER MAIN FUNCTION ================ | ------------------------------ | ----------------------------------------------- |
|                                               |                                |                                                 |
| 0. main_pset = mpi://WORLD                    |                                |                                                 |
| 1. Check if mpi://WORLD is dynamic   ------------>  PSet1 = {p1, p2}           |                                                 |
|              No!                     <------------                             |                                                 |
| --------------------------------------------- | ------------------------------ | ----------------------------------------------- |
| 2. Create an MPI Comm from main_pset <------------  PSet1 = {p1, p2}           |                                                 |
|    -> PSet1 -> comm_orig = {p1, p2}           |                                |                                                 |
|-----------------------------------------      |------------------------------- | ----------------------------------------------- |  
| 3. MPI_PSETOP_GROW(PSet1, 2)          ----------->  PSet1 = {p1, p2}           | ========== ENTER MAIN FUNCTION ================ |
|                                               |                                |     0. main_pset = mpi://WORLD                  |
|                                               |     PSet2 = {p3, p4}  <-----------   1. Check if mpi://WORLD is dynamic          |
|                                               |                       ----------->      Yes!                                     |
|                                               |     PSet3 = {p1, p2, p3, p4}   |                                                 |
|                                               |                                |                                                 |
|-----------------------------------------      |------------------------------- | ----------------------------------------------- |
| 4. Publish name of new main_pset in           |     PSet1 = {p1, p2}           |                                                 |
|    dict of PSet2 ("main_pset -> PSet3)  --------->  PSet2 = {p3, p4} <-----------    2. Lookup name of new main_pset             |
|                                               |                      ----------->       -> main_pset = PSet3                     |
|                                               |     PSet3 = {p1, p2, p3, p4}   |                                                 |
|-----------------------------------------      | ------------------------------ | ----------------------------------------------- |
| 5. Diconnect from comm_orig                   |     PSet1 = {p1, p2}           |                                                 |
|                                               |     PSet2 = {p3, p4}           |                                                 |
| 6. Create an MPI_comm from new main_pset <--------  PSet3 = {p1, p2, p3, p4} ----->  3. Create an MPI_comm from new main_pset    |
|     -> PSet3 -> comm_grow = {p1, p2, p3, p4}  |                                |         -> PSet3 -> comm_grow = {p1, p2, p3, p4}|
|                                               |                                |                                                 |
| --------------------------------------------- | ------------------------------ | ----------------------------------------------- |
| 7. Diconnect from comm_grow                   |     PSet1 = {p1, p2}           |      4. Diconnect from comm_grow                |
|                                               |     PSet2 = {p3, p4}           |                                                 |
| 8. Terminate                                  |     PSet3 = {p1, p2, p3, p4}   |      5. Terminate                               |
|_______________________________________________|________________________________|_________________________________________________|
``` 

Some remarks:
  <ul>
   <li>     The URI "mpi://WORLD" refers to different PSets for the original and dynamic processes, i.e. PSet1 and PSet2 respectively.
            This is similar to the concept of disjoint MPI_COMM_WORLD in the MPI_Spawn model.
            "mpi://WORLD" is therefor only a local alias for the global URI of these PSets </li>
   <li>     Note that step 3 and 4 of the original processes will only be executed by 1 process. In our example this will be rank 0 of comm_orig.
            Alternatively, it could be the primary process of PSet1, by checking for "mpi_primary" in MPI_Session_get_pset_info(PSet1); </li>
   <li>     As step 3 and 4 are only executed by one of the original processes, at this point only this process knows about the name of the new main pset. 
            In our example this information could simply be shared via MPI_Bcast on comm_orig. However, to demonstrate the general case (e.g. for cases where no such communicator exists),
            we will use `MPI_Session_dyn_v2a_psetop_query` and `MPI_Session_get_pset_data` to retrieve this information. </li> 
 </ul>

### 1. Headers and Helpers
``` 
#include "mpi.h"
#include <stdio.h>
#include <cstring>
#include <stdlib.h>
#include <unistd.h>

void free_string_array(char **array, int size){
    for(int i = 0; i < size; i++){
        free(array[i]);
    }
    free(array);
}
``` 

### 2. Program Entry Point
At the entry point of dynamic applications we need to differentiate between original and dynamically added processes.

In this example, the MPI processes:
<ul>
  <li> Initialize MPI </li>
  <li> Determine if they were added dynamically </li> 
      <ul>
            <li> If yes: Lookup kickoff information in the PSet Dictionary </li>
      </ul>
  <li> Establish communication </li>
</ul> 

```
/* Example of adding MPI processes */
int main(int argc, char* argv[]){

      MPI_Group group = MPI_GROUP_NULL;
      MPI_Session session = MPI_SESSION_NULL;
      MPI_Comm comm = MPI_COMM_NULL;
      MPI_Info info = MPI_INFO_NULL;
      char main_pset[MPI_MAX_PSET_NAME_LEN];
      char boolean_string[16], nprocs[] = "2", **input_psets, **output_psets, host[64];  
      int original_rank, new_rank, flag = 0, dynamic_process = 0, noutput, op;
      gethostname(host, 64);
      char *dict_key = strdup("main_pset"); // The key used to store the name of the new main PSet in the PSet Dictionary

      /* We start with the mpi://WORLD PSet as main PSet */
      strcpy(main_pset, "mpi://WORLD");

      /* Initialize the MPI Session */
      MPI_Session_init(MPI_INFO_NULL, MPI_ERRORS_ARE_FATAL, &session);

      /* Get the info from our mpi://WORLD pset */
      MPI_Session_get_pset_info (session, main_pset, &info);

      /* get value for the 'mpi_dyn' key -> if true, this process was added dynamically */
      MPI_Info_get(info, "mpi_dyn", 6, boolean_string, &flag);
      MPI_Info_free(&info);

      /* if mpi://WORLD is a dynamic PSet retrieve the name of the main PSet stored on mpi://WORLD */
      if(dynamic_process = (flag && 0 == strcmp(boolean_string, "True"))){
          /* Lookup the value for the "main_pset" key in the PSet Dictionary and use it as our main PSet */
          MPI_Session_get_pset_data (session, main_pset, main_pset, (char **) &dict_key, 1, true, &info);
          MPI_Info_get(info, "main_pset", MPI_MAX_PSET_NAME_LEN, main_pset, &flag);
          MPI_Info_free(&info);
      }

      /* create a communcator from our main PSet */
      MPI_Group_from_session_pset (session, main_pset, &group);
      MPI_Comm_create_from_group(group, "mpi.forum.example", MPI_INFO_NULL, MPI_ERRORS_RETURN, &comm);
      MPI_Comm_rank(comm, &original_rank);
      MPI_Group_free(&group);

      printf("Rank %d: Host = '%s', Main PSet = '%s'. I am '%s'!\n", original_rank, host, main_pset, dynamic_process ? "dynamic" : "original");
``` 

### 3. Increase the number of processes and switch to a new communicator

The original processes will now request 2 more processes and establish communication with the new processes.
In this example rank 0 is responsible for requesting the set operation, publish the kickoff information and finalizing the set operation.
All processes will then lookup the name of the new main pset, disconnect from the old communicator and create a new one.  
``` 
      /* Original processes will switch to a grown communicator */
      if(!dynamic_process){
          /* One process needs to request the set operation and publish the kickof information */
          if(original_rank == 0){

              /* Request the GROW operation */
              op = MPI_PSETOP_GROW;

              /* We add nprocs = 2 processes*/
              MPI_Info_create(&info);
              MPI_Info_set(info, "mpi_num_procs_add", nprocs);

              /* The main PSet is the input PSet of the operation */
              input_psets = (char **) malloc(1 * sizeof(char*));
              input_psets[0] = strdup(main_pset);
              noutput = 0;

              /* Send the Set Operation request */
              MPI_Session_dyn_v2a_psetop(session, &op, input_psets, 1, &output_psets, &noutput, info);
              MPI_Info_free(&info);

              /* Publish the name of the new main PSet on the delta Pset */
              MPI_Info_create(&info);
              MPI_Info_set(info, "main_pset", output_psets[1]);
              MPI_Session_set_pset_data(session, output_psets[0], info);
              MPI_Info_free(&info);
              free_string_array(input_psets, 1);
              free_string_array(output_psets, noutput);
          }

          /* All processes can query the information about the pending Set operation */      
          MPI_Session_dyn_v2a_query_psetop(session, main_pset, main_pset, &op, &output_psets, &noutput);

          /* Lookup the name of the new main PSet stored on the delta PSet */
          MPI_Session_get_pset_data (session, main_pset, output_psets[0], (char **) &dict_key, 1, true, &info);
          MPI_Info_get(info, "main_pset", MPI_MAX_PSET_NAME_LEN, main_pset, &flag); 
          free_string_array(output_psets, noutput);
          MPI_Info_free(&info);

          /* Disconnect from the old communicator */
          MPI_Comm_disconnect(&comm);

          /* create a new ommunicator from the new main PSet*/
          MPI_Group_from_session_pset (session, main_pset, &group);
          MPI_Comm_create_from_group(group, "mpi.forum.example", MPI_INFO_NULL, MPI_ERRORS_RETURN, &comm);
          MPI_Comm_rank(comm, &new_rank);
          MPI_Group_free(&group);

          /* Indicate completion of the Pset operation*/
          if(original_rank == 0){
              MPI_Session_dyn_finalize_psetop(session, main_pset);
          }

          printf("Rank %d: Host = '%s', Main PSet = '%s'. I am 'original'!\n", new_rank, host, main_pset);
      }
``` 

### 4. Safely Terminate
Processes need to disconnect from the communicator and finalize the MPI Session before terminating.
```

    MPI_Barrier(comm);

    /* Disconnect from the old communicator */
    MPI_Comm_disconnect(&comm);

    /* Finalize the MPI Session */
    MPI_Session_finalize(&session);

    return 0;
    
}
```

### 5. Compile the program
Compile the MPI program as usual.

### 6. Run program
You can now run your first dynamic MPI application with the following command:
```
mpirun -np 2 --display map --host n01:2,n02:2 path/to/executable
```

Hooray! You have run your first dynamic MPI application! The output should look like this:
```
========================   JOB MAP   ========================
Data for JOB prterun-n01-667301@1 offset 0 Total slots allocated 4
    Mapping policy: BYCORE:NOOVERSUBSCRIBE  Ranking policy: SLOT Binding policy: CORE:IF-SUPPORTED
    Cpu set: N/A  PPR: N/A  Cpus-per-rank: N/A  Cpu Type: CORE


Data for node: n01	Num slots: 2	Max slots: 0	Num procs: 2
        Process jobid: prterun-n01-667301@1 App: 0 Process rank: 0 Bound: package[0][core:0]
        Process jobid: prterun-n01-667301@1 App: 0 Process rank: 1 Bound: package[0][core:1]

=============================================================
Rank 0: Host = 'n01', Main PSet = 'mpi://WORLD'. I am 'original'!
Rank 1: Host = 'n01', Main PSet = 'mpi://WORLD'. I am 'original'!
Rank 1: Host = 'n01', Main PSet = 'prrte://base_name/1'. I am 'original'!
Rank 0: Host = 'n01', Main PSet = 'prrte://base_name/1'. I am 'original'!
Rank 3: Host = 'n02', Main PSet = 'prrte://base_name/1'. I am 'dynamic'!
Rank 2: Host = 'n02', Main PSet = 'prrte://base_name/1'. I am 'dynamic'!

```

## Contact
You want to experiment with this code? 
You have found a bug? 
You are interested in dicussing topics related to Dynamic Resources in HPC?

Do not hesitate to contact me:

domi.huber@tum.de


## Acknowledgements
This developement is part of a research project which has received funding from 
the Federal Ministry of Education andResearch and the European HPC Joint Undertaking (JU) 
under grant agreement No955701, Time-X. The JU receives support from the European Union's Horizon 2020
research and innovation programme and Belgium, France, Germany, Switzerland.

This development has been inspired greatly by the discussions of the MPI Sessions WG.
